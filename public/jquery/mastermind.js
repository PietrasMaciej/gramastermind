$().ready(function () {
  var fromSvr;

    $("#przeslij").click(function () {
      var info = {
        codeSize: $("#codeSize").val(),
        maxColors: $("#maxColors").val(),
        maxMoves: $("#maxMoves").val()
      };

    $.ajax({
      url: '/newgame',
      type: 'POST',
      data: JSON.stringify(info),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function() {}
    });
    $("#codeSize").prop('disabled', true);
    $("#maxColors").prop('disabled', true);
    $("#maxMoves").prop('disabled', true);
    $("#przeslij").prop('disabled', true);

    for(var i = 0; i < info.codeSize; i++) {
      $("<input type='text' id='zgaduj' />").insertAfter(".aqq");
    }

      $("#lotto").show();
  });

  $("#lotto").click(function () {
    var inserted = {
      move: []
    };

    $("#zgadywanka input[id='zgaduj']").each(function () {
      inserted.move.push($(this).val());
    });

    $.ajax({
      url: '/move',
      type: 'POST',
      data: JSON.stringify(inserted),
      contentType: 'application/json; charset=UTF-8',
      dataType: 'json',
      async: false,
      success: function (serw) {
        fromSvr = serw;
      }
    });
$("<p>Twój ruch to: "+inserted.move+"</p>").insertAfter(".aqq");
$("<p>Czarne: "+ fromSvr.black + ", białe: " + fromSvr.white+ "</p>").insertAfter(".aqq");
if(fromSvr.black === fromSvr.codeSize) {
  $("<p>Wygrałes!!!</p>").insertBefore(".aqq");
  $("#zgadywanka input[id='zgaduj']").each(function () {
     $(this).prop('disabled', true);
     $("#lotto").prop('disabled', true);
   });
}

if(fromSvr.miss === fromSvr.maxMoves) {
  $("<p>Przegrałeś</p>").insertBefore(".aqq");
  $("#zgadywanka input[id='zgaduj']").each(function () {
     $(this).prop('disabled', true);
     $("#lotto").prop('disabled', true);
   });
}
    //$("<p>" + ( fromSvr.maxMoves ) + "</p>" ).insertAfter( ".aqq ");
  });

});
