/*jshint -W117*/
var express = require('express');
var bodyParser = require('body-parser');
var _ = require('underscore');
var ejs = require('ejs');
var cookieSession = require('cookie-session');

var app = express();

app.use(express.static("public"));
app.use(bodyParser.json());
app.set('views', 'public');
app.use('/js', express.static('bower_components/jquery/dist'));
app.set('view engine', 'ejs');
app.use(cookieSession({secret: 'magicPass'}));

var game = {
  codeSize: 0,
  maxColors: 0,
  maxMoves: 0,
  secret: [],
  inserted: [],
  black: 0,
  white: 0,
  miss: 0
};

app.get('/', function (req, res) {
  res.render('index');
});

app.post('/newgame', function (req, res, next) {
  if(game.hasOwnProperty("miss"))
    game.miss = 0;
  if (game.hasOwnProperty("secret"))
    game.secret = [];
  if(game.hasOwnProperty("black"))
      game.black = 0;
  if(game.hasOwnProperty("white"))
      game.white = 0;
  if (game.hasOwnProperty("codeSize"))
    game.codeSize = parseInt(req.body.codeSize, 10);
  if (game.hasOwnProperty("maxColors"))
    game.maxColors = parseInt(req.body.maxColors, 10);
  if (game.hasOwnProperty("maxMoves"))
    game.maxMoves = parseInt(req.body.maxMoves, 10);
    req.session.stat = game;
    var arr = _.range(0,   req.session.stat.maxColors, 1);
      req.session.stat.secret = _.sample(arr,   req.session.stat.codeSize);
    console.log(  req.session.stat.secret);
    next();
});

app.post('/move', function (req, res) {
  if (  req.session.stat.hasOwnProperty("inserted")) {
    for(var i = 0; i < req.body.move.length; i++) {
        req.body.move[i] = parseInt(req.body.move[i], 10);
      }
    req.session.stat.inserted = req.body.move;
}

  var mark = function (code, move) {
    var calcBlack = function () {
      return _.size(_.filter(code, function (value, index) {
        return value === move[index];
      }));
    };
    var calculateWhites = function () {
      return _.countBy( code, function (num) {
        return num;
      });
    };
    var whiteFinder = function (code, move) {
      var appearancesMap = calculateWhites (code);
      return function (element) {
        if(appearancesMap[element] > 0) {
          appearancesMap[element]--;
          return true;
        }
        return false;
      };
    };
    var calcWhite = function () {
      return _.filter (move, whiteFinder( code, move ));
    };

    return {
      black: calcBlack(),
      white: calcWhite ( code, move ).length - calcBlack ( code, move )
    };
  };
  req.session.stat.black = mark( req.session.stat.secret,  req.session.stat.inserted).black;
  req.session.stat.white = mark( req.session.stat.secret,  req.session.stat.inserted).white;
if (  req.session.stat.black !==  req.session.stat.codeSize) {
    req.session.stat.miss++;
}

if(  req.session.stat.miss ===   req.session.stat.codeSize) {
  console.log("Przegrana!");
}
  console.log(  req.session.stat);
  console.log(mark(  req.session.stat.secret,   req.session.stat.inserted));

res.json(  req.session.stat);
});

app.listen(8888, function () {
  console.log( 'serwer działa na porcie 8888');
});
